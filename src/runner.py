import hashlib
from typing import Any, Dict

import typer
import wandb

from src.distill import distill
from src.paths import MODEL_RUN_FOLDER
from src.types import ModelType


def main(  # pylint: disable=too-many-locals
    *,
    model_type: ModelType = typer.Option(...),
    epochs: int = 10,
    lr: float = 1e-3,
    alpha: float = 0.5,
    temperature: float = 2.0,
    k: float = 0.5,
):
    config = {
        "model-type": model_type.value,
        "epochs": epochs,
        "lr": lr,
        "alpha": alpha,
        "temperature": temperature,
        "k": k,
    }
    name = get_run_name(config)
    run = wandb.init(
        project="distillation-temperature",
        name=name,
        config=config,
    )

    print(wandb.config)
    print(wandb.run.name)

    model_type = ModelType(run.config["model-type"])
    epochs = run.config["epochs"]
    lr = run.config["lr"]
    alpha = run.config["alpha"]
    temperature = run.config["temperature"]
    k = run.config["k"]
    output_dir = MODEL_RUN_FOLDER / name

    output_dir.mkdir(parents=True, exist_ok=True)

    distill(
        teacher_id="optimum/roberta-large-finetuned-clinc",
        student_id="nreimers/MiniLMv2-L12-H384-distilled-from-RoBERTa-Large",
        dataset_id="clinc_oos",
        dataset_name="plus",
        output_dir=output_dir,
        epochs=epochs,
        per_device_train_batch_size=64,
        per_device_eval_batch_size=64,
        fp16=True,
        model_type=model_type,
        learning_rate=lr,
        alpha=alpha,
        temperature=temperature,
        k=k,
    )
    run.finish()


def get_run_name(config_dict: Dict[str, Any]) -> str:
    parameters = [
        (config_dict["model-type"], "model-type"),
        (round(config_dict["alpha"], 3), "alpha"),
        (round(config_dict["temperature"], 3), "temperature"),
    ]
    if config_dict["model-type"] == "scaling":
        parameters += [(round(config_dict["k"], 3), "k")]

    return "_".join(
        [f"{value}_{label}" for value, label in parameters]
        + [get_id_for_dict(config_dict)]
    )


def get_id_for_dict(config_dict: Dict[str, Any]) -> str:
    """This function creates a unique hash
    based on the initial config dictionary
    that is used to label the model."""

    unique_str = "".join(
        f"'{key}':'{value}';" for key, value in sorted(config_dict.items())
    )
    return hashlib.sha1(unique_str.encode("utf-8")).hexdigest()[:5]


if __name__ == "__main__":
    typer.run(main)
