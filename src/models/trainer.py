from abc import ABC, abstractmethod
from typing import Dict, Tuple, Union

import torch
from transformers import AutoModelForSequenceClassification, Trainer, TrainingArguments
from transformers.modeling_outputs import SequenceClassifierOutput

from src.models.loss import (
    scaling_cosine_loss,
    scaling_kl_loss,
    temperature_cosine_loss,
    temperature_kl_loss,
)


class DistillationTrainingArguments(TrainingArguments):
    def __init__(
        self,
        *args,
        alpha: float = 0.5,
        temperature: float = 2.0,
        k: float = 0.5,
        **kwargs
    ) -> None:
        super().__init__(*args, **kwargs)
        self.alpha = alpha
        self.temperature = temperature
        self.k = k


class DistillationTrainer(ABC, Trainer):
    def __init__(
        self, *args, teacher_model: AutoModelForSequenceClassification = None, **kwargs
    ) -> None:
        super().__init__(*args, **kwargs)
        self.teacher = teacher_model
        # place teacher on same device as student
        self._move_model_to_device(self.teacher, self.model.device)
        self.teacher.eval()

    def compute_loss(
        self,
        model: AutoModelForSequenceClassification,
        inputs: torch.Tensor,
        return_outputs: bool = False,
    ) -> Union[Tuple[torch.Tensor, torch.Tensor], torch.Tensor]:
        # compute student output
        student_outputs = model(**inputs)
        teacher_outputs = self.teacher_output(inputs)
        student_loss = self.student_loss(student_outputs)
        teacher_loss = self.teacher_loss(
            student_outputs=student_outputs, teacher_outputs=teacher_outputs
        )
        loss = self.combine_loss(student_loss=student_loss, teacher_loss=teacher_loss)
        return (loss, student_outputs) if return_outputs else loss

    @torch.inference_mode()
    def teacher_output(
        self, inputs: Dict[str, torch.Tensor]
    ) -> SequenceClassifierOutput:
        return self.teacher(**inputs)

    def student_loss(  # pylint: disable=no-self-use
        self, output: SequenceClassifierOutput
    ) -> torch.Tensor:
        return output.loss

    @abstractmethod
    def teacher_loss(
        self,
        student_outputs: SequenceClassifierOutput,
        teacher_outputs: SequenceClassifierOutput,
    ) -> torch.Tensor:
        raise NotImplementedError()

    def combine_loss(
        self, student_loss: torch.Tensor, teacher_loss: torch.Tensor
    ) -> torch.Tensor:
        student_loss = self.args.alpha * student_loss
        teacher_loss = (1.0 - self.args.alpha) * teacher_loss
        return student_loss + teacher_loss


class KLTemperatureTrainer(DistillationTrainer):
    def teacher_loss(
        self,
        student_outputs: SequenceClassifierOutput,
        teacher_outputs: SequenceClassifierOutput,
    ) -> torch.Tensor:
        # assert size
        assert student_outputs.logits.size() == teacher_outputs.logits.size()

        return temperature_kl_loss(
            student_outputs=student_outputs,
            teacher_outputs=teacher_outputs,
            temperature=self.args.temperature,
        )


class CosineTemperatureTrainer(DistillationTrainer):
    def teacher_loss(
        self,
        student_outputs: SequenceClassifierOutput,
        teacher_outputs: SequenceClassifierOutput,
    ) -> torch.Tensor:
        # assert size
        assert student_outputs.logits.size() == teacher_outputs.logits.size()

        return temperature_cosine_loss(
            student_outputs=student_outputs,
            teacher_outputs=teacher_outputs,
            temperature=self.args.temperature,
        )


class KLScalingTrainer(DistillationTrainer):
    def teacher_loss(
        self,
        student_outputs: SequenceClassifierOutput,
        teacher_outputs: SequenceClassifierOutput,
    ) -> torch.Tensor:
        # assert size
        assert student_outputs.logits.size() == teacher_outputs.logits.size()

        return scaling_kl_loss(
            student_outputs=student_outputs,
            teacher_outputs=teacher_outputs,
            temperature=self.args.temperature,
            k=self.args.k,
        )


class CosineScalingTrainer(DistillationTrainer):
    def teacher_loss(
        self,
        student_outputs: SequenceClassifierOutput,
        teacher_outputs: SequenceClassifierOutput,
    ) -> torch.Tensor:
        # assert size
        assert student_outputs.logits.size() == teacher_outputs.logits.size()

        return scaling_cosine_loss(
            student_outputs=student_outputs,
            teacher_outputs=teacher_outputs,
            temperature=self.args.temperature,
            k=self.args.k,
        )
