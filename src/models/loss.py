import torch
import torch.nn.functional as F
from transformers.modeling_outputs import SequenceClassifierOutput


def temperature_kl_loss(
    student_outputs: SequenceClassifierOutput,
    teacher_outputs: SequenceClassifierOutput,
    temperature: float,
) -> torch.Tensor:
    assert student_outputs.logits.size() == teacher_outputs.logits.size()

    loss = F.kl_div(
        input=F.log_softmax(student_outputs.logits / temperature, dim=-1),
        target=F.softmax(teacher_outputs.logits / temperature, dim=-1),
        reduction="batchmean",
    )
    loss *= temperature**2
    return loss


def temperature_cosine_loss(
    student_outputs: SequenceClassifierOutput,
    teacher_outputs: SequenceClassifierOutput,
    temperature: float,
) -> torch.Tensor:
    assert student_outputs.logits.size() == teacher_outputs.logits.size()

    batch_size = student_outputs.logits.shape[0]
    loss = F.cosine_embedding_loss(
        input1=student_outputs.logits / temperature,
        input2=teacher_outputs.logits / temperature,
        target=torch.ones(batch_size, device=student_outputs.logits.device),
        reduction="mean",
    )
    return loss


def scaling_kl_loss(
    student_outputs: SequenceClassifierOutput,
    teacher_outputs: SequenceClassifierOutput,
    temperature: float,
    k: float,
) -> torch.Tensor:
    assert student_outputs.logits.size() == teacher_outputs.logits.size()

    loss = F.kl_div(
        input=torch.log(
            scale(logits=student_outputs.logits, temperature=temperature, k=k)
        ),
        target=scale(logits=teacher_outputs.logits, temperature=temperature, k=k),
        reduction="batchmean",
    )
    loss *= temperature**2
    return loss


def scaling_cosine_loss(
    student_outputs: SequenceClassifierOutput,
    teacher_outputs: SequenceClassifierOutput,
    temperature: float,
    k: float,
) -> torch.Tensor:
    assert student_outputs.logits.size() == teacher_outputs.logits.size()

    batch_size = student_outputs.logits.shape[0]
    loss = F.cosine_embedding_loss(
        input1=scale(logits=student_outputs.logits, temperature=temperature, k=k),
        input2=scale(logits=teacher_outputs.logits, temperature=temperature, k=k),
        target=torch.ones(batch_size, device=student_outputs.logits.device),
        reduction="mean",
    )
    return loss


def scale(
    logits: torch.Tensor,
    temperature: float,
    k: float,
) -> torch.Tensor:
    batch_size, output_size = logits.shape

    logits = logits.softmax(dim=1)
    logit_indices = logits.argsort(dim=1, descending=True)

    # when selecting multiple unaligned indices from a 2d tensor you can pass two lists
    # they are joined by index to determine the value to select:
    #
    #   t = torch.tensor(range(9)).reshape(3, 3)
    #   >>> tensor([[0, 1, 2],
    #   >>>         [3, 4, 5],
    #   >>>         [6, 7, 8]])
    #
    #   t[[0, 1], [2, 0]]
    #   >>> tensor([2, 3])
    #
    # You can see that this has selected indices (0,2)=2 and (1,0)=3 rather than (0,1)=1 and (2,0)=6
    # This can be extended to select multi dimensionally:
    #
    #   t[[[0], [1]], [[2], [0]]]
    #   >>> tensor([[2],
    #   >>>         [3]])
    #
    # The shape of the output is the same as the shape of the index lookup

    # the single index is used when selecting a single index per batch
    single_index = torch.tensor(range(batch_size), device=logits.device)

    # the remaining index is used when selecting one less than all indices per batch
    rest_index = single_index[:, None].broadcast_to(batch_size, output_size - 1)

    # these all have shape [batch_size, 1]
    top_probability = logits[single_index, logit_indices[:, 0]][:, None]
    remaining_probability = 1 - top_probability

    second_probability = logits[single_index, logit_indices[:, 1]][:, None]

    top_probability_change = top_probability - second_probability
    top_probability_change /= 1 + (second_probability / remaining_probability)
    top_probability_change *= 1 - k ** (temperature - 1)

    # this has a shape [batch_size, output_size-1]
    rest_probability = logits[rest_index, logit_indices[:, 1:]]
    rest_probability_change = (
        top_probability_change * rest_probability / remaining_probability
    )

    # if these changes are applied directly to logits then it breaks back propagation
    # generating an offset tensor that can be added to logits works fine
    offset = torch.zeros_like(logits)
    offset[single_index, logit_indices[:, 0]] = -top_probability_change[:, 0]
    offset[rest_index, logit_indices[:, 1:]] = rest_probability_change

    return logits + offset
