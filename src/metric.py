from typing import Dict

import numpy as np
from datasets import load_metric
from transformers import EvalPrediction

accuracy_metric = load_metric("accuracy")


def compute_metrics(eval_pred: EvalPrediction) -> Dict[str, float]:
    predictions, labels = eval_pred
    predictions = np.argmax(predictions, axis=1)
    acc = accuracy_metric.compute(predictions=predictions, references=labels)
    return {
        "accuracy": acc["accuracy"],
    }
