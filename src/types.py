from enum import Enum


class ModelType(str, Enum):
    KL_TEMPERATURE = "temperature"
    COSINE_TEMPERATURE = "cosine-temperature"
    KL_SCALING = "scaling"
    COSINE_SCALING = "cosine-scaling"
