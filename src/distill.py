import logging
import sys
from pathlib import Path

from transformers import (
    AutoModelForSequenceClassification,
    AutoTokenizer,
    DataCollatorWithPadding,
)

from src.data import load_dataset
from src.metric import compute_metrics
from src.models.trainer import (
    CosineScalingTrainer,
    CosineTemperatureTrainer,
    DistillationTrainingArguments,
    KLScalingTrainer,
    KLTemperatureTrainer,
)
from src.types import ModelType


def distill(  # pylint: disable=too-many-locals
    *,
    teacher_id: str,
    student_id: str,
    dataset_id: str,
    dataset_name: str,
    output_dir: Path,
    epochs: int,
    per_device_train_batch_size: int,
    per_device_eval_batch_size: int,
    fp16: bool,
    model_type: ModelType,
    learning_rate: float,
    alpha: float,
    temperature: float,
    k: float,
) -> None:
    logging.basicConfig(
        level=logging.getLevelName("INFO"),
        handlers=[logging.StreamHandler(sys.stdout)],
        format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
    )

    tokenizer = AutoTokenizer.from_pretrained(teacher_id)
    dataset, id2label, label2id = load_dataset(
        dataset_id=dataset_id,
        dataset_name=dataset_name,
        tokenizer=tokenizer,
        student_id=student_id,
    )

    # define training args
    training_args = DistillationTrainingArguments(
        output_dir=output_dir,
        num_train_epochs=epochs,
        per_device_train_batch_size=per_device_train_batch_size,
        per_device_eval_batch_size=per_device_eval_batch_size,
        fp16=fp16,
        learning_rate=learning_rate,
        seed=33,
        # logging & evaluation strategies
        logging_dir=output_dir / "logs",
        logging_strategy="epoch",
        evaluation_strategy="epoch",
        save_strategy="epoch",
        save_total_limit=2,
        load_best_model_at_end=True,
        metric_for_best_model="accuracy",
        report_to="wandb",
        # distilation parameters
        alpha=alpha,
        temperature=temperature,
        k=k,
    )

    data_collator = DataCollatorWithPadding(tokenizer=tokenizer)
    teacher_model = AutoModelForSequenceClassification.from_pretrained(
        teacher_id, num_labels=len(id2label), id2label=id2label, label2id=label2id
    )
    student_model = AutoModelForSequenceClassification.from_pretrained(
        student_id, num_labels=len(id2label), id2label=id2label, label2id=label2id
    )

    if model_type == ModelType.KL_SCALING:
        trainer_class = KLScalingTrainer
    elif model_type == ModelType.COSINE_SCALING:
        trainer_class = CosineScalingTrainer
    elif model_type == ModelType.KL_TEMPERATURE:
        trainer_class = KLTemperatureTrainer
    elif model_type == ModelType.COSINE_TEMPERATURE:
        trainer_class = CosineTemperatureTrainer
    else:
        raise AssertionError(f"Unsupported model type: {model_type.value}")

    trainer = trainer_class(
        model=student_model,
        args=training_args,
        teacher_model=teacher_model,
        train_dataset=dataset["train"],
        eval_dataset=dataset["validation"],
        data_collator=data_collator,
        tokenizer=tokenizer,
        compute_metrics=compute_metrics,
    )

    trainer.train()

    trainer.save_model(output_dir / "best_model")
