from typing import Any, Dict, Tuple

import datasets
from transformers import AutoTokenizer


def load_dataset(
    dataset_id: str, dataset_name: str, tokenizer: AutoTokenizer, student_id: str
) -> Tuple[datasets.DatasetDict, Dict[int, str], Dict[str, int]]:
    _check_tokenizers(teacher_tokenizer=tokenizer, student_id=student_id)

    dataset = datasets.load_dataset(dataset_id, dataset_name)

    # process dataset
    def process(examples: Dict[str, Any]) -> Dict[str, Any]:
        tokenized_inputs = tokenizer(examples["text"], truncation=True, max_length=512)
        return tokenized_inputs

    tokenized_datasets = dataset.map(process, batched=True)
    tokenized_datasets = tokenized_datasets.rename_column("intent", "labels")

    labels = tokenized_datasets["train"].features["labels"].names
    id2label = dict(enumerate(labels))
    label2id = {label: label_id for label_id, label in id2label.items()}

    return tokenized_datasets, id2label, label2id


def _check_tokenizers(teacher_tokenizer: AutoTokenizer, student_id: str) -> None:
    student_tokenizer = AutoTokenizer.from_pretrained(student_id)

    sample = "This is a basic example, with different words to test."
    teacher_tokens = teacher_tokenizer(sample)
    student_tokens = student_tokenizer(sample)

    assert teacher_tokens == student_tokens, "Tokenizers are not compatible"
